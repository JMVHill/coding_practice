function zhuLiDoTheThing(start, end) {
  let total = 0;
  for (let i = start; i <= end; i += 1) {
    if (i % 2 === 0) {
      total += i;
    }
  }

  return total;
}
