function zhuLiDoTheThing(limit) {
  let total = 0;
  for (let i = 0; i <= limit; i += 1) {
    if (i % 2 === 0) {
      total += i;
    }
  }

  if (total === 0) {
    return 0;
  }

  const count = Math.max(0, limit / 2);
  return total / count;
}
