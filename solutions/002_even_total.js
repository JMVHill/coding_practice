
function zhuLiDoTheThing_01(nValue) {
  let total = 0;
  for (let i = 1; i <= nValue; i += 1) {
    if (i % 2 === 0) {
      total += i;
    }
  }

  return total;
}

function zhuLiDoTheThing_01(nValue) {
  // Make a new empty array of "nValue" items long
  return new Array(nValue)
    // Fill the array with "undefined"
    .fill(undefined)
    // Loop over the array content and add events to "accumulator" aka "acc"
    .reduce((acc, _, v) => acc + (v % 2 === 0 ? v : 0), 0)
}
