# #1 Counting

Create a function which returns an array of numbers from 0 to "countTo"

```js
/**
 * @param countTo - Number up to which we are outputting
 * @return Array of numbers going from 0 to "countTo"
 */
function zhuLiDoTheThing(countTo) {
  // Your code here
}
```

### Example Runs

| Invocation            | Output            |
|-----------------------|-------------------|
| `zhuLiDoTheThing(4)`  | `[0, 1, 2, 3, 4]` |
| `zhuLiDoTheThing(2)`  | `[0, 1, 2]`       |
| `zhuLiDoTheThing(0)`  | `[0]`             |
| `zhuLiDoTheThing(-1)` | `[]`              |
