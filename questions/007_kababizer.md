# #7 Kababizer

Create a function which takes one argument "source" and turns it into Kababcase value.
Kababcase is where:
- All letters are lowercase
- There are no spaces, only hyphens

```js
/**
 * @param source - Input text to turn into kababcase
 * @return String representing a correctly formatted kababcase value based on "source"
 */
function zhuLiDoTheThing(source) {
  
}
```

### Example Runs

| Invocation                                          | Output                         |
|-----------------------------------------------------|--------------------------------|
| `zhuLiDoTheThing('this is a test')`                 | `'this-is-a-test'`             |
| `zhuLiDoTheThing('Some hybrid-text')`               | `'some-hybrid-test'`           |
| `zhuLiDoTheThing('alread-in-the-format')`           | `'alread-in-the-format'`       |
| `zhuLiDoTheThing('value with multiple     spaces')` | `'value-with-multiple-spaces'` |
| `zhuLiDoTheThing('input with numbers 0 1 2')`       | `'input-with-numbers-0-1-2'`   |
