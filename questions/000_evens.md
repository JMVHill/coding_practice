# #0 Evens

Create a function which takes one input "value"
The function then returns the `true` if the number is even and `false` if the number is odd

```js
/**
 * @param value - Number to check if this is even
 * @return Boolean indicating whether the provided value is even or not
 */
function zhuLiDoTheThing(value) {
  
}
```

### Example Runs

| Invocation             | Output  |
|------------------------|---------|
| `zhuLiDoTheThing(0)`   | `true`  |
| `zhuLiDoTheThing(1)`   | `false` |
| `zhuLiDoTheThing(8)`   | `true`  |
| `zhuLiDoTheThing(987)` | `false` |
| `zhuLiDoTheThing(560)` | `true`  |
