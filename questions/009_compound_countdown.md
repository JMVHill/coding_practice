# #9 Compound Countdown

Create a function which takes two arguments "values" and "target".
The function returns an array of the math operations which are performed for "values" in order to produce "target".
The operations are either addition ('+'), subtraction ('-'), multiplication ('*') or division ('/').
This function ignores the rules of BIDMAS, illustrated in the below following example.

```js
/**
 * @param values - The numeric values of the equation
 * @param target - The result of the equation
 * @return Array of character representing which operation should be applied to produce the target
 */
function zhuLiDoTheThing(values, target) {
  
}
```

### Example
```
Inputs zhuLiDoTheThing([20, 5, 2], 30)
        -> 20 - 5 = 15
        -> 15 * 2 = 30 

This gives the result ['-', '*'] and the equation looks like 20 - 5 * 2 = 30.
This equation does not follow the rules of BIDMAS, but we conveniently ignore this fact.
```

### Hint
```js
function checkAnswer(values, target, operations) {
  // There should be 1 more value than there are operations
  if (values.length - 1 !== operations.length) {
    return false;
  }
  
  let calculatedResult = values[0];
  for (let i = 0; i < operations.length; i += 1) {
    const operator = operations[i];
    
    if (operator === '-') {
      calculatedResult = calculatedResult - values[i + 1]
    }
    
    if (operator === '+') {
      calculatedResult = calculatedResult + values[i + 1]
    }
    
    if (operator === '*') {
      calculatedResult = calculatedResult * values[i + 1]
    }
    
    if (operator === '/') {
      calculatedResult = calculatedResult / values[i + 1]
    } 
  }
  
  return calculatedResult === target;
}

checkAnswer([1, 2], 3, ['+']); // This gives "true" if the calculation is correct 
```

### Example Runs

| Invocation                                 | Output                 |
|--------------------------------------------|------------------------|
| `zhuLiDoTheThing([1, 2], 3)`               | `['+']`                |
| `zhuLiDoTheThing([3, 3], 1)`               | `['/']`                |
| `zhuLiDoTheThing([2, 8], 10)`              | `['+']`                |
| `zhuLiDoTheThing([3, 4], -1)`              | `['-']`                |
| `zhuLiDoTheThing([20, 5, 4], 11)`          | `['-', '-']`           |
| `zhuLiDoTheThing([3995, 47, 5, 2, 2], 30)` | `['/', '/', '-', '*']` |
