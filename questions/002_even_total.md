# #2 Even Total

Create a function which takes all even numbers from 1 to "limit" and outputs the total

```js
/**
 * @param limit - Number up to which we are summing even numbers
 * @return Total of all even numbers up to, and including "limit"
 */
function zhuLiDoTheThing(limit) {
  
}
```

### Example Runs

| Invocation              | Output      |
|-------------------------|-------------|
| `zhuLiDoTheThing(4)`    | `6`         |
| `zhuLiDoTheThing(2)`    | `2`         |
| `zhuLiDoTheThing(0)`    | `0`         |
| `zhuLiDoTheThing(-2)`   | `0`         |
| `zhuLiDoTheThing(7)`    | `12`        |
| `zhuLiDoTheThing(5236)` | `6,851,306` |
