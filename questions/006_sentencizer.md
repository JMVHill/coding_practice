# #6 Sentencizer

Create a function which takes one argument "source" and turns it into sentence format.
Sentence format is where:
- The first letter of the sentence starts with a capital letter
- The sentence ends with a full stop
- All letters other than the first letter are lower case

```js
/**
 * @param source - Input text to turn into sentence format
 * @return String representing a correctly formatted sentence based on "source"
 */
function zhuLiDoTheThing(source) {
  
}
```

### Example Runs

| Invocation                              | Output                 |
|-----------------------------------------|------------------------|
| `zhuLiDoTheThing('this is a test')`     | `'This is a test.'`    |
| `zhuLiDoTheThing('tHIS IS A TESt')`     | `'This is a test.'`    |
| `zhuLiDoTheThing('a second test.')`     | `'A second test.'`     |
| `zhuLiDoTheThing('Already formatted.')` | `'Already formatted.'` |
| `zhuLiDoTheThing('CAPITALIZED')`        | `'Capitalized.'`       |
