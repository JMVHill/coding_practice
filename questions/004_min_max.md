# #4 Min Max

Create a function which takes an array of items called "values".
The function then returns the min and max values of items in the array.

```js
/**
 * @param values - An array of numeric values
 * @return Array containing two values, the first being the smallest and the second being the largest
 */
function zhuLiDoTheThing(values) {
  
}
```

### Example Runs

| Invocation                                  | Output       |
|---------------------------------------------|--------------|
| `zhuLiDoTheThing([1, 2, 3, 4, 5])`          | `[1, 5]`     |
| `zhuLiDoTheThing([5, 6, 7, 8])`             | `[5, 8]`     |
| `zhuLiDoTheThing([5, 4, 3, 2, 1])`          | `[1, 5]`     |
| `zhuLiDoTheThing([5, 73, 9, 12, 39])`       | `[5, 73]`    |
| `zhuLiDoTheThing([2, 8, 19, -30, 90, 420])` | `[-30, 420]` |
