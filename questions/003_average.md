# #3 Average

Create a function which takes a number we will call "limit".
The function then calculates the average value of all numbers added together from 0 to the provided limit

```js
/**
 * @param limit - Number up to which we are calculating the average
 * @return Average of all numbers up to, and including "limit"
 */
function zhuLiDoTheThing(limit) {
  
}
```

### Example Runs

| Invocation              | Output               |
|-------------------------|----------------------|
| `zhuLiDoTheThing(4)`    | `3`                  |
| `zhuLiDoTheThing(2)`    | `2`                  |
| `zhuLiDoTheThing(-2)`   | `0`                  |
| `zhuLiDoTheThing(7)`    | `3.4285714285714284` |
| `zhuLiDoTheThing(5236)` | `2619`               |
| `zhuLiDoTheThing(0)`    | `0`                  |
