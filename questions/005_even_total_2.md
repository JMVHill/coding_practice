# #5 Even Total 2

Create a function which takes two inputs "start" and "end"
The function then returns the total of all even numbers between start and end

```js
/**
 * @param start - Number from which we start counting
 * @param end - Upper limit of what we will sum even numbers to
 * @return Total of all even numbers between start & end (inclusive)
 */
function zhuLiDoTheThing(start, end) {
  
}
```

### Example Runs

| Invocation                  | Output   |
|-----------------------------|----------|
| `zhuLiDoTheThing(0, 4)`     | `6`      |
| `zhuLiDoTheThing(1, 5)`     | `6`      |
| `zhuLiDoTheThing(0, 10)`    | `30`     |
| `zhuLiDoTheThing(8, 20)`    | `98`     |
| `zhuLiDoTheThing(560, 987)` | `165422` |
