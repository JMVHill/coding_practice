# #8 Demi Countdown

Create a function which takes three arguments "valueA", "valueB" and "target".
The function returns the math operation which is performed for "valueA" and "valueB" in order to get "target".
The operation is either addition ('+'), subtraction ('-'), multiplication ('*') or division ('/').

```js
/**
 * @param valueA - The first number of the equation
 * @param valueB - The second number of the equation
 * @param target - The result of the equation
 * @return A character representing which operation should apply to produce the target
 */
function zhuLiDoTheThing(valueA, valueB, target) {
  
}
```

### Example Runs

| Invocation                      | Output |
|---------------------------------|--------|
| `zhuLiDoTheThing(1, 2, 3)`      | `'+'`  |
| `zhuLiDoTheThing(3, 3, 1)`      | `'/'`  |
| `zhuLiDoTheThing(2, 8, 16)`     | `'*'`  |
| `zhuLiDoTheThing(3, 4, 7)`      | `'+'`  |
| `zhuLiDoTheThing(20, 5, 15)`    | `'-'`  |
| `zhuLiDoTheThing(3995, 47, 85)` | `'/'`  |
